package slogctx_test

import (
	"context"
	"io"
	"log/slog"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/matthewhughes/slogctx"
)

func TestWithLogger(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	ctx := context.Background()

	ctx = slogctx.WithLogger(ctx, logger)

	require.Equal(t, slogctx.FromContext(ctx), logger)
}

func TestFromContext_Default(t *testing.T) {
	ctx := context.Background()

	require.Equal(t, slogctx.FromContext(ctx), slog.Default())
}

func TestHasLogger(t *testing.T) {
	for _, tc := range []struct {
		desc     string
		ctx      context.Context //nolint:containedctx
		expected bool
	}{
		{
			"without logger",
			context.Background(),
			false,
		},
		{
			"with logger",
			slogctx.WithLogger(context.Background(), slog.New(slog.NewTextHandler(io.Discard, nil))),
			true,
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			require.Equal(t, tc.expected, slogctx.HasLogger(tc.ctx))
		})
	}
}

func TestEnsureLogger(t *testing.T) {
	t.Run("persists existing logger", func(t *testing.T) {
		logger := slog.New(slog.NewTextHandler(io.Discard, nil))
		otherLogger := slog.New(slog.NewTextHandler(io.Discard, nil))

		ctx := slogctx.WithLogger(context.Background(), logger)
		setCtx := slogctx.EnsureLogger(ctx, otherLogger)

		require.Equal(t, ctx, setCtx)
		require.Equal(t, logger, slogctx.FromContext(setCtx))
	})

	t.Run("adds logger if none set", func(t *testing.T) {
		logger := slog.New(slog.NewTextHandler(io.Discard, nil))

		ctx := context.Background()
		setCtx := slogctx.EnsureLogger(ctx, logger)

		require.NotEqual(t, ctx, setCtx)
		require.Equal(t, logger, slogctx.FromContext(setCtx))
	})
}

func Example() {
	replacer := func(groups []string, a slog.Attr) slog.Attr {
		// for simple output, only log the message
		if a.Key != slog.MessageKey {
			return slog.Attr{}
		}
		return a
	}
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{ReplaceAttr: replacer}))

	ctx := slogctx.WithLogger(context.Background(), logger)
	slogctx.FromContext(ctx).Info("logging stuff")

	// Output:
	// msg="logging stuff"
}
