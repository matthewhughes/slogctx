package slogctx

import (
	"context"
	"log/slog"
)

type loggerKey struct{}

// WithLogger returns a copy of parent which the given logger set as a value.
// The logger can be accessed via [FromContext].
func WithLogger(parent context.Context, logger *slog.Logger) context.Context {
	return context.WithValue(parent, loggerKey{}, logger)
}

// HasLogger reports whether ctx has a logger set via [WithLogger].
func HasLogger(ctx context.Context) bool {
	return ctx.Value(loggerKey{}) != nil
}

// FromContext returns the logger set on ctx via [WithLogger],
// or [slog.Default] if no logger has been set.
func FromContext(ctx context.Context) *slog.Logger {
	if logger, ok := ctx.Value(loggerKey{}).(*slog.Logger); ok {
		return logger
	}
	return slog.Default()
}

// EnsureLogger returns a context that has a logger set via [WithLogger].
// It returns the passed in context if it already has a logger set,
// otherwise it returns a new context.
func EnsureLogger(ctx context.Context, logger *slog.Logger) context.Context {
	if HasLogger(ctx) {
		return ctx
	}
	return WithLogger(ctx, logger)
}
