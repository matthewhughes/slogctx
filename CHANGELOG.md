# Changelog

## 0.2.0 - 2024-11-01

### Fixed

  - Fixed key used to store in context was vulnerable to collisions from other
    libraries

## 0.1.0 - 2024-04-30

Initial release
