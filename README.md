# `slogctx`

[![Go
Reference](https://pkg.go.dev/badge/gitlab.com/matthewhughes/slogctx.svg)](https://pkg.go.dev/gitlab.com/matthewhughes/slogctx)

Helpers for setting and accessing
[`slog.Logger`](https://pkg.go.dev/log/slog#Logger) on
[`context.Context`](https://pkg.go.dev/context#Context)
